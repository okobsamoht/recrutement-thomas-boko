# THOMAS BOKO

## WEB
Déploirmrnt de la partie web:

```bash
cd webBootstrap
docker build -t TBWEB-image:v1
docker run -d -p 8080:80 TBWEB-image:v1
```

Pointer le navidateur sur le port 8080

## MOBILE
Pour exécuter et tester dans le navigateur:

```bash
cd mobileIonic
docker build -t TBMOB-image:v1
docker run -d -p 8188:8100 TBMOB-image:v1
```

Pointer le navidateur sur le port 8188

## Usage
